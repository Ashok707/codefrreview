define(function(){
    angular
        //.module('coreModule')
        //.registerController('LoginController', ['$rootScope', '$scope', '$themeModule', '$location', '$AuthenticationService',
        .module('controllerModule')
        .register.controller('LoginController', ['$rootScope', '$scope', '$themeModule', '$location', '$AuthenticationService',
            'AlertService', '$sessionStorage', 'gettextCatalog', 'Restangular',
            function ($rootScope, $scope, $theme, $location, $AuthenticationService, AlertService, $sessionStorage, gettextCatalog, Restangular) {
                'use strict';
                $theme.set('fullscreen', true);
                $rootScope.isLoginPage = true;
                $scope.$on('$destroy', function() {
                    $theme.set('fullscreen', false);
                });
                $scope.Error = "";
                var vm = this;
                var username;
                var password;
                $scope.isloginFailed = false;
                //$scope.username="Admin";
                //$scope.password="admin123";
                $scope.login = function login() {

                    $scope.dataLoading = true;
                    username = $scope.username;
                    password = $scope.password;
                    
                  //  var LoggedInUser = { 'UserName': "Admin", 'UserId': "1" };

                     if (!($scope.username == "" || $scope.password) == "") {
                        $AuthenticationService.Login(username, password,
                            function (LoggedInUser) {
                                authenticateUserComplete(LoggedInUser);
                            });
                     } else {
                         $scope.isloginFailed = true;
                        return false;
}
                };
                
                (function initController() {
                    if($rootScope.ChosenLanguage==undefined){
                        $rootScope.ChosenLanguage = "en";
                        $rootScope.ChosenLanguageDesc = "English";
                        $sessionStorage.ChosenLanguage = "en";
                        $sessionStorage.ChosenLanguageDesc = "English";
                    }
                    // reset login status
                    //$AuthenticationService.ClearCredentials();
                })();


                function authenticateUserComplete(LoggedInUser) {
                    if (LoggedInUser.UserId > 0) {
                        //$AuthenticationService.SetCredentials(username, password);
                        $rootScope.IsLoggedIn = true;
                        $scope.dataLoading = true;

                        Restangular.all('menu').post(LoggedInUser.plain()).then(
                            function (response) {
                                var DefaultMenuUrl;
                                var DefaultLoadPage;
                                $rootScope.menu = response.data;
                                if ($rootScope.menu.length > 0) {
                                    DefaultMenuUrl = $rootScope.menu[0].DefaultMenuUrl;
                                }

                                if (DefaultMenuUrl != null && DefaultMenuUrl != undefined & DefaultMenuUrl != "") {
                                    $location.path(DefaultMenuUrl)
                                } else {
                                    $location.path('/InventorySearch');
                                }
                            },
                            function (response) {
                                //AlertService.ExceptionAlert("Error Retriving Menu :" + response.data);
                            });                       
                    } else {
                      //  AlertService.simpleError('Error','Login failed.');
                        $scope.dataLoading = false;
                        $rootScope.IsLoggedIn = false;
                        $scope.isloginFailed = true;
                    }
                }

                function authenticateUserError(response) {
                    alert('login failed');
                    $scope.Error("Login Failed");
                }
                $scope.languageOptions = {
                    dataSource: $rootScope.languages,
                    dataTextField: "desc",
                    dataValueField: "code",
                    select: onSelect,
                    valueTemplate: '<span class="selected-value"></span><span>{{dataItem.desc}}</span>',
                    value: $sessionStorage.ChosenLanguage,
                    template: '<flag country="{{dataItem.country}}" size="16"></flag>&nbsp;&nbsp;<span>{{dataItem.desc}}</span>',
                };  
                function onSelect(e) {
                    var dataItem = this.dataItem(e.item);
                    $rootScope.ChosenLanguage = dataItem.code;
                    $rootScope.ChosenLanguageDesc = dataItem.desc;
                    $sessionStorage.ChosenLanguage = dataItem.code;
                    $sessionStorage.ChosenLanguageDesc = dataItem.desc;
                    gettextCatalog.setCurrentLanguage($rootScope.ChosenLanguage);
                };
        }]);
});
