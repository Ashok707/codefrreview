﻿define(function () {
    var controllerModule = angular.module('controllerModule');
    controllerModule.register.controller('addStockController', ['$scope', 'Restangular', '$sessionStorage', 'IdleTimeOutService', 'AlertService', '$location', '$timeout', 'CodeMasterService', function ($scope, Restangular, $sessionStorage, IdleTimeOutService, AlertService, locationattr, timeout, CodeMasterService) {
        //  IdleTimeOutService.start();
       // timeout(function () { $scope.CallTabLoad(); }, 2000);
        $scope.currency;
        $scope.ItemCategory;
        $scope.categorydatasource = [];
        $scope.categoryfilter = [];
        $scope.ProductItems = [];
        $scope.categoryItems = [];
        $scope.expirysetting = '';
        $scope.textValue = '';
        $scope.ItemName = '';
        $sessionStorage.InventorySearchData = "";


        $scope.CallTabLoad = function () {
            var tabIndx = -1;
            var searchID = locationattr.search().Id;
            var productID = locationattr.search().PId;

            var mainobject = $scope.search(searchID, "ID", $scope.categorydatasource);
            var mainobjectIndex = $scope.searchIndex(searchID, "ID", $scope.categorydatasource);
            var combobox = $("#Category_ID").data("kendoComboBox");
            combobox.select(mainobjectIndex);
            combobox.trigger("change");
            console.log(mainobject, 'mainobject');
            var subobject = $scope.search(productID, "ProductID", mainobject.productcategory);
            if (subobject != null) {
                // $scope.categoryfilter = subobject.ProductName;
                $scope.addToGrid(subobject.ProductID, subobject.ProductName);
                $('#products').val(subobject.ProductName);
                $scope.keyupProductFilter();
                //$scope.$apply();
            }


        };

        Restangular.one('Inventory/GetCategoryList/addstock').get().then(
                function (response) {
                    $scope.categorydatasource = response.data;

                    $scope.CategoryComboBoxOption = {
                        placeholder: " Select Category ",
                        dataTextField: "Name",
                        dataValueField: "ID",
                        dataSource: $scope.categorydatasource,
                        close: function (e) {
                            // handle the event
                        },
                        filter: "StartsWith",
                        height: 400,
                        select: function (e) {
                            if (e.item) {
                                var data = this.dataItem(e.item.index());
                                $scope.ProductItems = data.productcategory;
                                $scope.ItemName = data.Name;
                            }
                        },
                        change: function (e) {
                            var value = this.value();
                            //  console.log(e, 'change');
                            var data = this.dataItem(this.selectedIndex);
                            $scope.ProductItems = data.productcategory;
                            // Use the value of the widget
                        }

                    };
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.CallTabLoad();
                        });
                    }, 2000);

                },
                function (response) {
                    //alert(response);
                    AlertService.simpleError('Error', response.data);
                }
                );

        $scope.keyupProductFilter = function () {
            var val = $('#products').val();
            $scope.textValue = val;
            if (val.length > 0) {
                $scope.categoryItems = $scope.ProductItems.filter(function (obj) {
                    if ((obj.ProductName.toUpperCase().startsWith(val.toUpperCase()))
                        || (obj.Manufacturer.toUpperCase().startsWith(val.toUpperCase())) ||
                       (obj.Number.toUpperCase().startsWith(val.toUpperCase()))) {
                        return obj;
                    }
                });
            }
            else {
                $scope.categoryItems = "";
            }
        };


        $scope.startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        }

        $scope.search = function (nameKey, prop, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                console.log(i);
                if (myArray[i][prop] == nameKey) {
                    return myArray[i];
                }
            }
        }

        $scope.searchIndex = function (nameKey, prop, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                console.log(i);
                if (myArray[i][prop] == nameKey) {
                    return i;
                }
            }
        }
        Restangular.one('Inventory/GetWareHouseList').get().then(
           function (response) {
               $scope.warehousedataSource = response.data;
           },
           function (response) {
               //alert(response);
               AlertService.simpleError('Error', response.data);
           }
           );

        currency_type();
        function currency_type() {
            var keys = ["COUNTRY", "CURRENCY_CODE"];
            CodeMasterService.getSettings(keys, null, 'SINGLE',//MULTIPLE
               function (response) {
                   $scope.currency = response.data;
                   console.log($scope.currency.Value1);
                   $scope.currencyname = $scope.currency.Value1;
                   $scope.AddStockGrid = {
                       dataSource: new kendo.data.DataSource({
                           pageSize: 7,
                           autoSync: true,
                           schema: {
                               model: {
                                   id: "id",
                                   fields: {
                                       id: { editable: false },
                                       Quantity: { type: "number" },
                                       Batch: { type: "string" },
                                       ExpiryDate: { type: "date" },
                                       Bin: { type: "string", editable: true },
                                       WareHouse: { type: "string", editable: true },
                                   }
                               }
                           },
                           aggregate: [
                             { field: "TotalCost", aggregate: "sum" },
                           ],
                       }),
                       pageable: true,
                       height: 480,
                       columns: [
                             { field: "Item", width: 10, width: "70px", editable: false },
                             {
                                 field: "WareHouse",
                                 width: "50px",

                                 editor: function (container, options) {
                                     var input = $("<input input  class='form-control' Tabindex='1'  ng-bind='WareHouse' />");
                                     input.attr("name", options.field);
                                     input.appendTo(container);
                                     input.kendoComboBox({
                                         placeholder: "Select...",
                                         dataSource: $scope.warehousedataSource,
                                         dataTextField: "Name",
                                         dataValueField: "id",
                                         change: onChange(options.model.id, options.model.WareHouse)
                                     });
                                 },
                             },
                             {
                                 field: "Bin",
                                 width: "50px",
                                 // template: "<input  class='form-control' style='width:75px;height:20px'>",
                                 editor: function (container, options) {
                                     var input = $("<input  Tabindex='2' ng-bind='Bin' />");
                                     input.attr("name", options.field);
                                     input.appendTo(container);
                                     var row = input.closest("tr");
                                     var grid = row.closest("[data-role=grid]").data("kendoGrid");
                                     var dataItem = grid.dataItem(row);
                                     var newBaseUrl = Restangular.configuration.baseUrl;
                                     input.kendoComboBox({
                                         placeholder: "Select...",
                                         dataSource: {
                                             transport: {
                                                 serverFiltering: true,
                                                 read: {
                                                     dataType: 'json',
                                                     url: newBaseUrl + '/Inventory/GetBinList/' + dataItem.WareHouse
                                                 }
                                             }
                                         },

                                         dataTextField: "Name",
                                         dataValueField: "Name"

                                     });
                                 },
                             },
                             { field: "ExpiryDate", title: "Expiry Date", format: "{0:dd-MMM-yyyy}", width: "75px", editor: dateTimeEditor },


                             {
                                 field: "Quantity",
                                 template: "<input class='form-control' data-bind='value: Quantity' type='number' min='0' onchange='caltotalfromqty(#=id#,this.value)' style='width:45px;height:20px;'>",
                                 width: "35px",

                             },
                             {
                                 field: "Batch",
                                 template: "<input class='form-control' data-bind='value: Batch' id='batchtext' style='width:75px;height:20px;'></input>",
                                 width: "50px",
                                 editable: false,
                             },
                             //{
                             //    field: "ExpiryDate",
                             //    title: "Expiry Date",
                             //    template: "<input class='form-control' data-bind='value: ExpiryDate' type='date' id='ExpiryDate' style='width:130px;height:20px;'>",
                             //    width: "75px",
                             //    editable: false,
                             //},

                             {
                                 field: "UnitCost",
                                 title: "Unit Cost",
                                 template: "<input class='form-control' min='0' data-bind='value: UnitCost' id='unitcostid' type='number' onchange='caltotalfromCost(#=id#,this.value)'  style='width:60px;height:20px';>",
                                 width: "45px",
                                 editable: false,
                                 editor: noneditor,
                                 footerTemplate: 'Grand Total:'
                             },
                             {
                                 field: "TotalCost",
                                 title: "Total Cost",
                                 template: "<input data-bind='value: TotalCost' class='textlabel' style='width:60px;'></input>",
                                 width: "60px",
                                 editable: false,
                                 footerTemplate: '#=window.calculatePriceAggregate()#'

                             },
                             { command: ["destroy"], title: "&nbsp;", width: "50px" }
                       ],
                       editable: true,
                       dataBound: function () {
                           var rows = this.tbody.children();
                           var dataItems = this.dataSource.view();
                           for (var i = 0; i < dataItems.length; i++) {
                               kendo.bind(rows[i], dataItems[i]);
                           }
                       },
                   };

               },
                       function (response) {
                       });
        };

        var keys = ["APP_DEFAULTS", "ADD_STOCK"];
        CodeMasterService.getSettings(keys, null, 'SINGLE',//MULTIPLE
          function (response) {
              $scope.allowexpiry = response.data;
              console.log($scope.allowexpiry.Value1);
              $scope.expirysetting = $scope.allowexpiry.Value1;
          },

                  function (response) {
                  });

        function dateTimeEditor(container, options) {

            $('<input data-text-field="' + options.field + '" data-value-field="' + options.field +
          '" data-bind="value:' + options.field + '" data-format="' + options.format + '" />')
              .appendTo(container)
              .kendoDateTimePicker({
                  disableDates: function (date) {
                      if ($scope.expirysetting.toUpperCase() == 'YES') {

                      }
                      else {
                          return date < new Date();
                      }
                  }

              });
        }

        function noneditor(container, options) {
            container.text(options.model[options.field]);
            container.removeClass("k-edit-cell");
        };

        window.calculatePriceAggregate = function () {
            var grid = $("#addstockgrid").data("kendoGrid");
            var data = grid.dataSource.data();
            var total = 0;
            for (var i = 0; i < data.length; i++) {
                total += data[i].TotalCost;
            }
            return total;
        };

        $scope.removearray = function (name, value) {
            if (subgriddataitems.length > 0) {
                if ($scope.checkAndAdd(value)) {
                    var array = $.map(subgriddataitems, function (v, i) {
                        return v[name] === value ? null : v;
                    });
                    subgriddataitems.length = 0; //clear original array
                    subgriddataitems.push.apply(this, array); //push all elements except the one we want to delete
                }
            }
        };

        $scope.checkAndAdd = function (ProductID) {
            var found = subgriddataitems.some(function (el) {
                return el.ProductID === ProductID;
            });
            if (found)
                return true;
            else
                return false;
        }


        $scope.addToGrid = function (id, product) {
            var grid = $("#addstockgrid").data("kendoGrid");
            var dataSource = grid.dataSource;
            var today = new Date();
            var weekno = today.getWeek() + 1;
            var year = today.getFullYear();
            var isproductexist = "No";
            var data = dataSource.data();
            for (var i = 0; i < data.length ; i++) {
                if (data[i].id == id)
                    isproductexist = "Yes";
            }

            if (isproductexist == "No")
                dataSource.add({ id: id, Item: product, Quantity: "", Batch: "", UnitCost: "", TotalCost: 0, squnum: "#SQU-101", ExpiryWeek: weekno, ExpiryYear: year });
            else
                AlertService.simpleError("Warning", "Product already available in the grid");


        }

        angular.element(window).on('keydown', function (e) {
            console.log(e);
        });

        $scope.updategrid = function () {
            var grid = $("#addstockgrid").data("kendoGrid");
            var length = grid.dataSource.data().length;
            // $scope.cidx = 0;
            var today = new Date();
            var weekno = today.getWeek() + 1;
            var year = today.getFullYear();
            var isvalid = "Yes";
            var errormessage = "";
            $scope.issuccess = true;
            $scope.emessage = "";
            for (var i = 0; i < length; i++) {
                var productid = grid.dataSource.data()[i].id;
                var qty = grid.dataSource.data()[i].Quantity;
                var batch = grid.dataSource.data()[i].Batch;
                var unitCost = grid.dataSource.data()[i].UnitCost;
                var ExpiryDate = grid.dataSource.data()[i].ExpiryDate;
                var warehouse = grid.dataSource.data()[i].WareHouse;
                var Bin = grid.dataSource.data()[i].Bin;
                var ExpYear = 0; var Expdate = 0; var Expweek = 0;
                if (ExpiryDate != null) {
                    ExpYear = ExpiryDate.getFullYear();
                    Expdate = ExpiryDate.getFullYear() + '-' + (ExpiryDate.getMonth() + 1) + '-' + ExpiryDate.getDate();
                    Expweek = ExpiryDate.getWeek();
                }

                //alert(productid + ':' + qty + ':' + batch + ':' + unitCost + ' :' +  i);    
                //alert(Bin + ':' + warehouse + ':' + Expdate + ':' + ExpYear + ':' + Expweek);

                if ((warehouse == undefined) || (warehouse == "") || (warehouse.length == 0)) {
                    isvalid = "No";
                    errormessage = "warehouse should not be empty";

                }
                else if (warehouse.length > 0) {
                    var CheckWareHouse = $scope.warehousedataSource.filter(function (obj) {
                        if (obj.Name.toUpperCase() == warehouse.toUpperCase()) {
                            return obj;
                        }
                    });

                    if (CheckWareHouse == 0) {
                        var NotificationArray = [{ message: 'select a valid Warehouse', type: "Error" }];
                        NotificationArray.top = 50;
                        NotificationArray.left = 950;
                        NotificationArray.bottom = 0;
                        NotificationArray.right = 0;
                        AlertService.consolidatedNotificationFN(NotificationArray);
                        return false;
                    }



                    else if ((Bin.length == 0) || (Bin == undefined) || (Bin == "")) {
                        isvalid = "No";
                        errormessage = "Bin should not be empty";
                    }

                    else if (ExpiryDate == null) {
                        isvalid = "No";
                        errormessage = "Expiry Date should not be empty";
                    }

                    else if (ExpYear < year) {
                        isvalid = "No";
                        errormessage = "Expiry year must be greater than or equal to current year";
                    }
                    else if (((ExpYear == year) && (Expweek < weekno)) || (Expweek > 52)) {
                        isvalid = "No";
                        errormessage = "Expiry week must be greater than or equal to current year week";
                    }

                    else if ((qty <= 0) || (qty == undefined) || (qty == "")) {
                        isvalid = "No";
                        errormessage = "Quantity should not  be empty or not equal to zero";
                    }

                    else if ((batch.length == 0) || (batch == undefined) || (batch == "")) {
                        isvalid = "No";
                        errormessage = "Batch should not be empty";
                    }

                    else if ((unitCost <= 0) || (unitCost = undefined) || (unitCost = "")) {
                        isvalid = "No";
                        errormessage = "UnitCost should not  be empty or not equal to zero";
                    }

                    else if ($scope.ddlitemtype.Value2 == "SELECT") {
                        isvalid = "No";
                        errormessage = "Type of add must be select for Item ";
                    }

                }
            }

            if (isvalid == "Yes") {
                for (var i = 0; i < length; i++) {
                    var productid = grid.dataSource.data()[i].id;
                    var qty = grid.dataSource.data()[i].Quantity;
                    var batch = grid.dataSource.data()[i].Batch;
                    var unitCost = grid.dataSource.data()[i].UnitCost;
                    var ExpiryDate = grid.dataSource.data()[i].ExpiryDate;
                    var warehouse = grid.dataSource.data()[i].WareHouse;
                    var Bin = grid.dataSource.data()[i].Bin;
                    var ExpYear = ExpiryDate.getFullYear();
                    var Expdate = ExpiryDate.getFullYear() + '-' + (ExpiryDate.getMonth() + 1) + '-' + ExpiryDate.getDate();
                    var Expweek = ExpiryDate.getWeek();

                    var data = {
                        Warehouse: warehouse,
                        BinName: Bin,
                        ProductID: productid,
                        SupplierId: "3",
                        Qty: qty,
                        BatchNo: batch,
                        MRP: 10,
                        ExpiryWeek: Expweek,
                        ExpiryYear: ExpYear,
                        ExpiryDate: Expdate,
                        PurchaseOrderno: 10,
                        UnitCost: unitCost,
                        Remarks: 10,
                        CreatedBy: 1,
                        // PRH_TYPE: $scope.ddlitemtype.Name,
                        TRD_TRANSACTIONTYPE: $scope.ddlitemtype.Value2
                    };


                    $scope.AddStock(data, i, length);
                }

            }
            else {
                //alert(errormessage);
                var NotificationArray = [{ message: errormessage, type: "Error" }];
                NotificationArray.top = 50;
                NotificationArray.left = 950;
                NotificationArray.bottom = 0;
                NotificationArray.right = 0;
                AlertService.consolidatedNotificationFN(NotificationArray);
                return false;
            }
        }

        window.onChange = function (id, warehouse) {
            var grid = $("#addstockgrid").data("kendoGrid");
            for (var i = 0; i < grid.dataSource.data().length; i++) {
                if (parseInt(grid.dataSource.data()[i].id) == id) {
                    grid.dataSource.data()[i].Bin = "";
                    if (warehouse != undefined)
                        grid.dataSource.data()[i].WareHouse = warehouse;
                }
            }
        }

        $scope.cancelgrid = function () {
            itemcount = 0;
            $scope.itemcount = 0;
            $("#addstockgrid").data('kendoGrid').dataSource.data([]);
        }


        $scope.AddStock = function (stock, i, length) {
            Restangular.all('Inventory/AddStock').post(stock).then(
                      function (response) {
                          if (response.data != "success") {
                              $scope.issuccess = false;
                              $scope.emessage = response.data;
                          }
                          if (i == (length - 1)) {
                              if ($scope.issuccess == true) {
                                  //alert("Stock Added Successfully");
                                  var NotificationArray = [{ message: "Stock Added Successfully", type: "Success" }];
                                  NotificationArray.top = 50;
                                  NotificationArray.left = 950;
                                  NotificationArray.bottom = 0;
                                  NotificationArray.right = 0;
                                  AlertService.consolidatedNotificationFN(NotificationArray);

                                  // AlertService.simpleError('Success', "Stock Added Successfully");
                                  var pageURL = $(location).attr("href");
                                  var cpage = pageURL.substring(pageURL.indexOf('#/') + 2);
                                  pageURL = pageURL.replace(cpage, "inventorysearch")
                                  $(location).attr("href", pageURL);
                              }
                              else {
                                  //alert($scope.emessage);
                                  var NotificationArray = [{ message: $scope.emessage, type: "Error" }];
                                  NotificationArray.top = 50;
                                  NotificationArray.left = 950;
                                  NotificationArray.bottom = 0;
                                  NotificationArray.right = 0;
                                  AlertService.consolidatedNotificationFN(NotificationArray);
                                  //AlertService.simpleError('Error', $scope.emessage);
                              }
                          }
                      },
                     function (response) {
                         //alert(response);
                         AlertService.simpleError('Error', response.data);
                     }
                     );
        }

        $scope.NewItemClick = function () {
            var pageURL = $(location).attr("href");
            var cpage = pageURL.substring(pageURL.indexOf('#/') + 2);
            pageURL = pageURL.replace(cpage, "inventoryAdd?Name=" + $scope.textValue + "&ItemType=" + $scope.ItemName);
            $(location).attr("href", pageURL);
        };

        Date.prototype.getWeek = function () {
            var onejan = new Date(this.getFullYear(), 0, 1);
            var today = new Date(this.getFullYear(), this.getMonth(), this.getDate());
            var dayOfYear = ((today - onejan + 1) / 86400000);
            return Math.ceil(dayOfYear / 7)
        };
        load_ItemType();

        function load_ItemType() {
            var keys = ["ADD_STOCK", "STOCK_TYPE"];
            CodeMasterService.getSettings(keys, null, 'MULTIPLE',//MULTIPLE
               function (response) {
                   $scope.ItemType = response.data.plain();
                   $scope.ddlitemtype = $scope.ItemType[0];

               },
                       function (response) {
                       });
        };




        $scope.GetValue = function (itemtype) {
            var itemtypeId = $scope.ddlitemtype.Value2;
            var itemtypeName = $.grep($scope.ItemType, function (itemtype) {
                return itemtypeName == itemtypeId;
            });
            console.log($scope.ddlitemtype);

        }




    }]);


});