﻿using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using Unite.Domain.Core.Services;
using Unite.Web.Api.Filters;
using System.Web.Http.Cors;
using Unite.Web.Api.Models;
using System.Threading;
using System.Security.Principal;
using Unite.Web.Api.ActionFilters;
using Unite.Infrastructure;
using Unite.Domain.Core.Entities;

namespace Unite.Web.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //[AuthorizationRequired]
    public class AuthenticateController : CustomApiController
    {
        #region Private variable.

        private readonly ITokenServices _tokenServices;
        private readonly IUserServices _userServices;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public AuthenticateController(ITokenServices tokenServices, IUserServices userServices)
        {
            _tokenServices = tokenServices;
            _userServices = userServices;

        }

        #endregion

        /// <summary>
        /// Authenticates user and returns token with expiry.
        /// </summary>
        /// <returns></returns>
        ///         [HttpPost]
        [OverrideActionFilters]
        [Route("api/authenticate")]
        [Route("api/get/token")]
        public HttpResponseMessage Authenticate()
        {

            if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                var basicAuthenticationIdentity = System.Threading.Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                if (basicAuthenticationIdentity != null)
                {
                    LoginUser user = new LoginUser();
                    user.UserId = basicAuthenticationIdentity.UserId;
                    return GetAuthToken(user);
                }
            }
            return null;
        }

        /// <summary>
        /// Authenticates user and returns token with expiry.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OverrideActionFilters]
        [Route("api/login")]

        public HttpResponseMessage Authenticate(loginuser user)
        {
            var responseMessage = Request.CreateResponse(HttpStatusCode.OK, "Authentication Failed");

            LoginUser LoggedInUser = _userServices.Authenticate(user.username, user.password);
            if (LoggedInUser != null)
            {
                if (LoggedInUser.UserId > 0)
                {
                    var basicAuthenticationIdentity = new BasicAuthenticationIdentity(user.username, user.password);
                    if (basicAuthenticationIdentity != null)
                    {
                        basicAuthenticationIdentity.UserId = LoggedInUser.UserId;
                        var genericPrincipal = new GenericPrincipal(basicAuthenticationIdentity, null);
                        Thread.CurrentPrincipal = genericPrincipal;
                        return GetAuthToken(LoggedInUser);
                    }
                }
                else
                {
                    responseMessage.Headers.Add("Token", "");
                    responseMessage.Headers.Add("TokenExpiry", "0");
                    responseMessage.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
                }
            }

            return responseMessage;
        }

        /// <summary>
        /// Authenticates user and returns token with expiry.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/refreshtoken/{userId}")]
        [OverrideActionFilters]
        public HttpResponseMessage RefreshToken(long userId)
        {
            var responseMessage = Request.CreateResponse(HttpStatusCode.OK, "Authentication Failed");

            LoginUser _user = _userServices.GetLoginUser(userId);
            if (userId > 0)
            {
                var basicAuthenticationIdentity = new BasicAuthenticationIdentity(_user.UserName, _user.Password);
                if (basicAuthenticationIdentity != null)
                {
                    basicAuthenticationIdentity.UserId = userId;
                    var genericPrincipal = new GenericPrincipal(basicAuthenticationIdentity, null);
                    Thread.CurrentPrincipal = genericPrincipal;
                    return GetAuthToken(_user);
                }
            }
            else
            {
                responseMessage.Headers.Add("Token", "");
                responseMessage.Headers.Add("TokenExpiry", "0");
                responseMessage.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
            }

            return responseMessage;
        }

        /// <summary>
        /// Returns auth token for the validated user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private HttpResponseMessage GetAuthToken(LoginUser user)
        {
            var token = _tokenServices.GenerateToken(user.UserId);
            var response = Request.CreateResponse(user);
            response.Headers.Add("Token", token.AuthToken);
            response.Headers.Add("TokenExpiry", token.ExpiresOn.ToString("yyyy-MM-dd HH:mm:ss"));
            response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
            return response;
        }
    }
}
